package minesweeper.solver.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Objects;

@Data
@Builder
@EqualsAndHashCode
public class Field {
   private boolean isMine = false;
   private boolean isSatisfied = false;
   private boolean isFlagged = false;
   private boolean isFree = false;
   private boolean isUnopened = false;
   private int mines = -1;
   private int x;
   private int y;

}

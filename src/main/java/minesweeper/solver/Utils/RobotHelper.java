package minesweeper.solver.Utils;

import java.awt.*;

import static java.awt.event.InputEvent.BUTTON1_MASK;
import static java.awt.event.InputEvent.BUTTON3_MASK;

public class RobotHelper {
    private Robot robot;

    public RobotHelper(Robot robot) {
        this.robot = robot;
    }

    public void click(int x, int y, long delay) {
        robot.mouseMove(x, y);
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        robot.mousePress(BUTTON1_MASK);
        robot.mouseRelease(BUTTON1_MASK);
    }


    public void rclick(int x, int y, int delay) {
        robot.mouseMove(x, y);
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        robot.mousePress(BUTTON3_MASK);
        robot.mouseRelease(BUTTON3_MASK);
    }
}

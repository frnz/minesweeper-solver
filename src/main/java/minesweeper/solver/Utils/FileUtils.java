package minesweeper.solver.Utils;


import lombok.extern.slf4j.Slf4j;

import java.net.URL;

@Slf4j(topic = "Utils")
public class FileUtils {
    private static final String EXE_NAME = "MineSweeperXP.exe";

    private FileUtils() {
        throw new AssertionError("Class not intended for initialisation.");
    }

    public static ProcessBuilder loadMineSweeperExe(String path){
        log.info("Loading exe with path '{}'.",path);
        if (!path.contains(".exe"))path = path + EXE_NAME;

        URL mineSweeperUrl = ClassLoader.getSystemResource(path);
        return new ProcessBuilder(mineSweeperUrl.getFile());
    }
}

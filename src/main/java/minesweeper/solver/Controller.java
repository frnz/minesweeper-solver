package minesweeper.solver;

import lombok.extern.slf4j.Slf4j;
import minesweeper.solver.Utils.GetWindow;
import minesweeper.solver.Utils.RobotHelper;
import minesweeper.solver.model.Field;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class Controller {
    private static final int NUMBER_OF_ROWS = 16;
    private static final int NUMBER_OF_COLUMNS = 30;
    private static final int START_OFFSET_X = 23;
    private static final int START_OFFSET_Y = 108;
    private static final int OFFSET_X = 16;
    private static final int OFFSET_Y = 16;
    private static final long START_CLUSTER_BREAKPOINT = 20;

    private final MineSweeperLogic logic;
    private final Robot robot;
    private final RobotHelper r;

   public Controller(Robot robot) {
        this.robot = robot;
        this.r = new RobotHelper(robot);
        this.logic = new MineSweeperLogic(this.r);
    }

    public void start() {
        log.info("Start of the solver logic.");
        try {
            //get dimensions of window
            int[] rect = GetWindow.getRect("Minesweeper");
            int width = rect[2] - rect[0];
            int height = rect[3] - rect[1];
            log.info("The corner locations are {}. width={} height={}", Arrays.toString(rect), width, height);

            //click on the initial value
            do {
                r.click(rect[0] + START_OFFSET_X + 14 * OFFSET_X,
                        rect[1] + START_OFFSET_Y + 7 * OFFSET_Y, 10);
                List<Field> state = getGameState(rect[0], rect[1], width, height);
                if ((state.size() - state.stream().filter(Field::isUnopened).count()) > START_CLUSTER_BREAKPOINT) break;
                robot.keyPress(KeyEvent.VK_F2);
                robot.keyRelease(KeyEvent.VK_F2);
            } while (true);


            long unOpenedCount;
            long unOpenedCount_previousIteration = solveOneIteration(rect[0], rect[1], width, height);
            do {
                unOpenedCount = solveOneIteration(rect[0], rect[1], width, height);

                if (unOpenedCount_previousIteration == unOpenedCount) {
                    if (unOpenedCount < 10) {
                        Image stateImg = getGameImage(rect[0], rect[1], width, height);
                        try {
                            saveImageToFile(stateImg);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    robot.keyPress(KeyEvent.VK_F2);
                    robot.keyRelease(KeyEvent.VK_F2);
                    start();
                    // throw new RuntimeException("Cant find any more 100% Fields.");
                } else {
                    unOpenedCount_previousIteration = unOpenedCount;
                }

                log.info("Fields to go: {}", unOpenedCount);
            } while (unOpenedCount > 0);

        } catch (  GetWindow.GetWindowRectException | GetWindow.WindowNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void saveImageToFile(Image image) throws IOException {
        //save to file
        ImageIO.write((RenderedImage) image, "png", new File(
                LocalDateTime.now().toString()
                        .replace("-", "_")
                        .replace(".", "_")
                        .replace(":", "_") + "_screenshot.png"));
    }

    private long solveOneIteration(int startX, int startY, int width, int height) {
        List<Field> game = getGameState(startX, startY, width, height);
        log.info("Done with primary logic.");
        return logic.solve(game);
    }

    private List<Field> getGameState(int startX, int startY, int width, int height) {
        //take screenshot
        Rectangle location = new Rectangle(width, height);
        location.setLocation(startX, startY);
        logic.setInitX(startX + START_OFFSET_X);
        logic.setInitY(startY + START_OFFSET_Y);
        BufferedImage image = robot.createScreenCapture(location);

        List<Field> game = new ArrayList<>();

        //manipulate image
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            for (int j = 0; j < NUMBER_OF_ROWS; j++) {
                //click on the square
                  /*  r.click(rect[0]+START_OFFSET_X + i * OFFSET_X,
                            rect[1]+START_OFFSET_Y + j * OFFSET_Y,
                            10);*/
                //color it
                int targetLocationX = START_OFFSET_X + i * OFFSET_X;
                int targetLocationY = START_OFFSET_Y + j * OFFSET_Y;

                Field result = calcFieldFromImagePos(image, targetLocationX, targetLocationY, i, j, 16);
                game.add(result);
                //Thread.sleep(300);
            }
        }
        return game;
    }

    private Image getGameImage(int startX, int startY, int width, int height) {
        //take screenshot
        Rectangle location = new Rectangle(width, height);
        location.setLocation(startX, startY);
        logic.setInitX(startX + START_OFFSET_X);
        logic.setInitY(startY + START_OFFSET_Y);
        return robot.createScreenCapture(location);
    }

    private Field calcFieldFromImagePos(BufferedImage image, int targetLocationX, int targetLocationY, final int x, final int y, int size) {
        //size has to be even
        if (size % 2 != 0) size++;

        //get colors of surrounding pixel
        List<Integer> colors = new ArrayList<>();
        for (int i = -size / 2; i < size / 2; i++) {
            for (int j = -size / 2; j < size / 2; j++) {
                colors.add(image.getRGB(targetLocationX + i, targetLocationY + j));
            }
        }

        // calc average of this color
        int red = (int) colors.stream().map(Color::new).mapToInt(Color::getRed).average().orElse(1);
        int green = (int) colors.stream().map(Color::new).mapToInt(Color::getGreen).average().orElse(1);
        int blue = (int) colors.stream().map(Color::new).mapToInt(Color::getBlue).average().orElse(1);
        Color resultingColor = new Color(red, green, blue);

        //color shit
        for (int i = -size / 2; i < size / 2; i++) {
            for (int j = -size / 2; j < size / 2; j++) {
                colors.add(image.getRGB(targetLocationX + i, targetLocationY + j));
                image.setRGB(targetLocationX + i, targetLocationY + j, resultingColor.getRGB());
            }
        }

        Color f_nothing = new Color(195, 195, 195);
        Color f_free = new Color(184, 184, 184);
        Color f_1 = new Color(154, 154, 194);
        Color f_2 = new Color(132, 164, 132);
        Color f_3 = new Color(196, 134, 134);
        Color f_4 = new Color(138, 138, 166);
        Color f_5 = new Color(163, 128, 128);
        Color f_6 = new Color(126, 162, 162);
        Color f_7 = new Color(147, 147, 147);
        Color f_8 = new Color(161, 161, 161);
        Color f_mine = new Color(127, 127, 127);
        Color f_known_mine = new Color(182, 166, 166);
        Color f_mine_exploded = new Color(159, 30, 30);
        Color f_mine_exploded_2 = new Color(198, 184, 184);
        Color f_mine_wrong_guess = new Color(156, 110, 110);

        if (isColorSimilar(resultingColor, f_nothing, 10)) {
            return Field.builder().x(x).y(y).isUnopened(true).build();
        } else if (isColorSimilar(resultingColor, f_free, 10)) {
            return Field.builder().x(x).y(y).isFree(true).build();
        } else if (isColorSimilar(resultingColor, f_1, 10)) {
            return Field.builder().x(x).y(y).mines(1).build();
        } else if (isColorSimilar(resultingColor, f_2, 10)) {
            return Field.builder().x(x).y(y).mines(2).build();
        } else if (isColorSimilar(resultingColor, f_3, 10)) {
            return Field.builder().x(x).y(y).mines(3).build();
        } else if (isColorSimilar(resultingColor, f_4, 10)) {
            return Field.builder().x(x).y(y).mines(4).build();
        } else if (isColorSimilar(resultingColor, f_5, 10)) {
            return Field.builder().x(x).y(y).mines(5).build();
        } else if (isColorSimilar(resultingColor, f_6, 10)) {
            return Field.builder().x(x).y(y).mines(6).build();
        } else if (isColorSimilar(resultingColor, f_7, 10)) {
            return Field.builder().x(x).y(y).mines(7).build();
        } else if (isColorSimilar(resultingColor, f_8, 10)) {
            return Field.builder().x(x).y(y).mines(8).build();
        } else if (isColorSimilar(resultingColor, f_known_mine, 10)) {
            return Field.builder().x(x).y(y).isFlagged(true).isMine(true).build();
        } else if (isColorSimilar(resultingColor, f_mine_exploded, 10)) {
            throw new RuntimeException("You lost :(");
        } else if (isColorSimilar(resultingColor, f_mine, 10)
                || isColorSimilar(resultingColor, f_mine_exploded, 10)
                || isColorSimilar(resultingColor, f_mine_exploded_2, 10)
                || isColorSimilar(resultingColor, f_mine_wrong_guess, 10)) {
            return Field.builder().x(x).y(y).isMine(true).build();
        } else {
            throw new RuntimeException("Field (" + x + "," + y + ") " + resultingColor.getRGB() + " " + resultingColor + " is unknown");
        }
    }

    private boolean isColorSimilar(Color color, Color targetColor, int buffer) {
        return color.getRed() >= targetColor.getRed() - buffer
                && color.getRed() <= targetColor.getRed() + buffer
                && color.getGreen() >= targetColor.getGreen() - buffer
                && color.getGreen() <= targetColor.getGreen() + buffer
                && color.getBlue() >= targetColor.getBlue() - buffer
                && color.getBlue() <= targetColor.getBlue() + buffer;

    }
}

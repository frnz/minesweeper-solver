package minesweeper.solver;

import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.io.IOException;

import static minesweeper.solver.Utils.FileUtils.loadMineSweeperExe;

@Slf4j(topic = "AppStart")
public class Starter {

    public static void main(String[] args) throws AWTException, InterruptedException, IOException {
        loadMineSweeperExe("MineSweeperXp.exe").start();
        waitToOpenProgram();
        new Controller(new Robot()).start();
    }

    private static void waitToOpenProgram() throws InterruptedException {
        Thread.sleep(10L);
    }
}

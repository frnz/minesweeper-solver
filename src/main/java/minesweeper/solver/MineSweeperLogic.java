package minesweeper.solver;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import minesweeper.solver.Utils.RobotHelper;
import minesweeper.solver.model.Field;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Slf4j
public class MineSweeperLogic {
    private List<Field> game;
    private RobotHelper r;

    @Setter
    private int initX;
    @Setter
    private int initY;

    private static final int OFFSET_X = 16;
    private static final int OFFSET_Y = 16;

    public MineSweeperLogic(RobotHelper r) {
        this.r = r;
    }

    public long solve(List<Field> fieldList) {
        this.game = fieldList;
        setGameToFocus();

        findAllConvincedMines();
        triggerAllKnownMines();

        game.stream()
                .filter(Field::isUnopened)
                .filter(notAMine())
                .filter(f -> calcSurroundingFields(f.getX(), f.getY()).stream().anyMatch(Field::isSatisfied))
                .forEach(this::clickField);
        return countUnopenedFields();
    }

    private void findAllConvincedMines() {
        log.info("Satisfied Fields: {}", game.stream().filter(Field::isSatisfied).count());
        List<Field> numberField = game.stream().filter(f -> f.getMines() > 0).collect(Collectors.toList());
        for (Field nField : numberField) {
            calcSatisfaction(nField, calcSurroundingFields(nField.getX(), nField.getY()));
        }
        log.info("Satisfied Fields: {}", game.stream().filter(Field::isSatisfied).count());
    }

    private void triggerAllKnownMines() {
        game.stream()
                .filter(Field::isMine)
                .filter(f -> !f.isFlagged())
                .peek(System.out::println)
                .forEach(f -> r.rclick(initX + f.getX() * OFFSET_X, initY + f.getY() * OFFSET_Y, 1));
    }

    private void clickField(Field field) {
        r.click(initX + field.getX() * OFFSET_X, initY + field.getY() * OFFSET_Y, 1);
    }

    private Predicate<Field> notAMine() {
        return f -> !f.isMine();
    }

    private long countUnopenedFields() {
        return game.stream().filter(Field::isUnopened).count();
    }

    private void setGameToFocus() {
        r.click(initX - 10, initY - 10, 1);
    }

    private void calcSatisfaction(Field field, List<Field> surroundingFields) {
        long unopenedCount = surroundingFields.stream().filter(Field::isUnopened).count();
        long mineCount = surroundingFields.stream().filter(Field::isMine).count();
//        log.info("{} has unopened: {} and mines: {}",field.toString(),unopenedCount,mineCount);

        //all remaining fields are mines
        if (unopenedCount + mineCount == field.getMines()) {

            //log.info("{} is satisfied with  unopened: {} and mines: {}", field.toString(), unopenedCount, mineCount);
            surroundingFields.stream().filter(Field::isUnopened).forEach(f -> {
                game.stream().filter(f::equals).forEach(ff -> ff.setMine(true));
            });
            game.stream().filter(f -> f.getX() == field.getX() && f.getY() == field.getY()).forEach(f -> f.setSatisfied(true));
            //game.stream().filter(f->f.equals(field)).forEach(f->f.setSatisfied(true));
        } else if (mineCount == field.getMines()) {
            game.stream().filter(f -> f.getX() == field.getX() && f.getY() == field.getY()).forEach(f -> f.setSatisfied(true));
        }
    }

    private List<Field> calcSurroundingFields(int x, int y) {
        return game.stream()
                .filter(f -> f.getX() >= x - 1)
                .filter(f -> f.getX() <= x + 1)
                .filter(f -> f.getY() >= y - 1)
                .filter(f -> f.getY() <= y + 1)
                .filter(f -> !((f.getX() == x) && (f.getY() == y)))
                // .peek(f->log.info("Surrounding "+x+","+y+" is "+f.isSatisfied()))
                .collect(Collectors.toList());
    }

}
